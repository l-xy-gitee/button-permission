/**
 * v-auth
 * 按钮权限指令
 */
import { useAuthStore } from '@/stores/modules/auth'

const auth = {
  mounted(el, binding) {
    const { value } = binding
    const authStore = useAuthStore()
    let currentPageRoles = []
    const currentPage =
      authStore.authButtonList.filter((item) => item.menuUrl === authStore.curRouteInfo.path) || []
    currentPage.length > 0 && (currentPageRoles = currentPage[0].btnList)
    if (value instanceof Array && value.length) {
      const hasPermission = value.every((item) => currentPageRoles.includes(item))
      if (!hasPermission) el.remove()
    } else {
      if (!currentPageRoles.includes(value)) el.remove()
    }
  }
}

export default auth
