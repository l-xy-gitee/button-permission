import { computed } from 'vue'
import { useAuthStore } from '@/stores/modules/auth'

/**
 * @description 页面按钮权限
 * */
export const useAuthButtons = () => {
  const authStore = useAuthStore()
  let currentPageRoles = []
  const currentPage =
    authStore.authButtonList.filter((item) => item.menuUrl === authStore.curRouteInfo.path) || []
  currentPage.length > 0 && (currentPageRoles = currentPage[0].btnList)

  const BUTTONS = computed(() => {
    let currentPageAuthButton = {}
    currentPageRoles.forEach((item) => (currentPageAuthButton[item] = true))
    return currentPageAuthButton
  })
  return {
    BUTTONS
  }
}
