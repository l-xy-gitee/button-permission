import { defineStore } from 'pinia'

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    // 按钮权限列表
    authButtonList: [
      {
        menuUrl: '/a/b',
        menuId: 1,
        menuName: 'a-b',
        btnList: ['a:b:edit', 'a:b:delete']
      }
    ],
    // 当前页面信息
    curRouteInfo: { name: 'a-b', path: '/a/b', meta: { title: 'a-b' } },
    testObj: {}
  }),
  getters: {},
  actions: {
    // 设置当前菜单信息
    async setCurRouteInfo(data) {
      this.curRouteInfo = data
    },
    // 设置按钮权限列表
    setAuthButtonList(data) {
      this.authButtonList = data
    },
    setTestObj(data) {
      this.testObj = data
    }
  }
})
