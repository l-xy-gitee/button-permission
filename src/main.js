import { createApp } from 'vue'
// 自定义指令
import directives from '@/directives/index'
// pinia
import pinia from '@/stores'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(pinia)
app.use(router)
app.use(directives)

app.mount('#app')
